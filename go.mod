module gitlab.hamkorbank.uz/hamkor-mobile/calc_formula

go 1.17

require (
	github.com/PaesslerAG/gval v1.1.2 // indirect
	github.com/skx/evalfilter/v2 v2.1.17 // indirect
	github.com/skx/subcommands v0.9.1 // indirect
)
