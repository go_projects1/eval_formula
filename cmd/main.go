package main

import (
	"fmt"

	"github.com/PaesslerAG/gval"
	"github.com/skx/evalfilter/v2"
)

func main() {
  way2()
}

func way2() {
	sum := 1000000
	value, err := gval.Evaluate("0.5*sum/100",
		map[string]interface{}{
			"sum": sum,
		})
	if err != nil {
		fmt.Println(err)
	}
	comm := value.(float64)
	fmt.Print(comm)
}

func way1() {
	script := `return 0.5;`
	eval := evalfilter.New(script)
	err := eval.Prepare()

	if err != nil {
		fmt.Printf("Failed to compile the code:%s\n", err.Error())
	}
	dailySum := 100
	//res,err := eval.Run(dailySum)
	res,err:= eval.Execute(dailySum)
	if err != nil {
		panic(err)
	}
	fmt.Println(*(&res))
}